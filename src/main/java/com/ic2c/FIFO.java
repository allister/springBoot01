/**
 * 易码当先 <br>
 * client <br>
 * com.ic2c.client <br>
 * FIFOCache.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年1月6日-下午2:00:12 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * <br>
 * FIFOCache <br>
 * 
 * @author Allister.Liu(刘继鹏) <br>
 *         时间：2019年1月6日-下午2:00:12 <br>
 * @version 1.0.0
 * 
 */
public class FIFO {

	
	
	/**
	 * 
	 * 1、循环需要访问的页面序号
	 * 2、判断是否缺页中断（是否存在物理块中），如不存在，则缺页中断，根据FIFO移除停留时间最久的页面，然后插入此页，反之不中断
	 * 
	 * 
	 * com.ic2c.client <br>
	 * 方法名：FIFO<br>
	 * @author Allister.Liu(刘继鹏) <br>
	 * 时间：2019年1月6日-下午2:26:45 <br>
	 * @param maxize 物理块个数
	 * @param page	页面序号数组
	 * @return int<br>
	 * @exception <br>
	 * @since  1.0.0 <br>
	 */
	public static int FIFO(int maxize, int[] page) {
		
		int count = 0;
		HashMap<Integer, Integer> pages = new HashMap<Integer, Integer>();
		
		for (int j = 0; j < page.length; j++) {
			// 判断是否缺页
			if (pages.containsKey(Integer.valueOf(page[j]))) 
			{
				// 命中缓存  不缺页
				for (Entry<Integer, Integer> entry : pages.entrySet()) {
					Integer value = entry.getValue();
					entry.setValue(Integer.valueOf(value.intValue() + 1));
				}
				continue;
			} 
			else 
			{
				// 缺页
				// 如果缓存满了，则去除缓存中存在最久的页面
				if (pages.size() == maxize) {
					int max = 0;
					int key = 0;
					for (Entry<Integer, Integer> entry1 : pages.entrySet()) {
						// 求出缓存中存在最久的页面
						if (entry1.getValue().intValue() > max) {
							max = entry1.getValue().intValue();
							key = entry1.getKey().intValue();
						}
					}
					// 移除页面
					pages.remove(key);
				}
				// 就把当前缓存列表中页面的时间加1；
				for (Entry<Integer, Integer> entry : pages.entrySet()) {
					Integer value = entry.getValue();
					entry.setValue(Integer.valueOf(value.intValue() + 1));
				}
				// 将新的请求页面加入缓存列表中并将其请求次数设置为0
				pages.put(Integer.valueOf(page[j]), Integer.valueOf(0));
				// 统计缺页次数
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		int maxsize = 3;
		int[] page = { 7,  0,  1,  2,  3,  0,  4,  2,  3 };
		int count = FIFO(maxsize, page);
		System.out.println("缺页中断总数：" + count);
	}

}
